* @ValidationCode : MjotMTM1MDE0OTI5OTpjcDEyNTI6MTU3MTQyNjA1MzQ0Njpzc3ViYXNoY2hhbmRhcjoxOjA6MDoxOmZhbHNlOk4vQTpERVZfMjAxOTEwLjIwMTkwOTIwLTA3MDc6MTU6MTU=
* @ValidationInfo : Timestamp         : 19 Oct 2019 00:44:13
* @ValidationInfo : Encoding          : cp1252
* @ValidationInfo : User Name         : ssubashchandar
* @ValidationInfo : Nb tests success  : 1
* @ValidationInfo : Nb tests failure  : 0
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : 15/15 (100.0%)
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : DEV_201910.20190920-0707
*-----------------------------------------------------------------------------
$PACKAGE ST.TemenosHackathon
*
*Implementation of ST_TemenosHackathon.tTransactionSignStore
*
SUBROUTINE JWT.TRANSACTION.SIGN.STORE
* Developed By	:
* Program Name	: JWT.TRANSACTION.SIGN.STORE
* Module Name	: ST
* @package 		: ST_TemenosHackathon
*-----------------------------------------------------------------------------
* <desc>
* Program Description
* Template Definition for JWT.TRANSACTION.SIGN.STORE
* </desc>
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------
*
*17/10/2019 - Enhancement Temenos - Hackathon
*				Table Definition for JWT.TRANSACTION.SIGN.STORE
*
*-----------------------------------------------------------------------------
* <region name= Inserts>
	$USING EB.Template
	$USING EB.SystemTables

* </region>
*-----------------------------------------------------------------------------
	EB.Template.setTableName('JWT.TRANSACTION.SIGN.STORE') ;* Full application name including product prefix
    EB.Template.setTableTitle('JWT TRANSACTION SIGN STORE')  ;* Screen title
	EB.Template.setTableStereotype('H')                ;* H, U, L, W or T
    EB.Template.setTableProduct('ST')                  ;* Must be on EB.PRODUCT
    EB.Template.setTableSubproduct('')                 ;* Must be on EB.SUB.PRODUCT
    EB.Template.setTableClassification('INT')          ;* As per FILE.CONTROL
    EB.Template.setTableSystemclearfile('')            ;* As per FILE.CONTROL
	EB.Template.setTableRelatedfiles('')               ;* Related Files
	EB.Template.setTableIspostclosingfile('')          ;* postCLosing File
	EB.Template.setTableEquateprefix('ST.JWT')         ;* Use to create field with prefix.
    EB.Template.setTableIdprefix('')                   ;* Table Id prefix
*-----------------------------------------------------------------------------
    EB.Template.setTableBlockedfunctions('V')          ;* Space delimited list of blocked functions
*-----------------------------------------------------------------------------
	EB.Template.setTableTriggerfield('')               ;* Trigger Field
    EB.SystemTables.setCNsOperation('') ;* Enable NS

RETURN
END

