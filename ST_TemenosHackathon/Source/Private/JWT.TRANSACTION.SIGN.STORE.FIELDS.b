* @ValidationCode : MjotOTMwNjU5NjY2OmNwMTI1MjoxNTcxNDI2MDMxNjYwOnNzdWJhc2hjaGFuZGFyOi0xOi0xOjA6MTp0cnVlOk4vQTpERVZfMjAxOTEwLjIwMTkwOTIwLTA3MDc6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Oct 2019 00:43:51
* @ValidationInfo : Encoding          : cp1252
* @ValidationInfo : User Name         : ssubashchandar
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : true
* @ValidationInfo : Compiler Version  : DEV_201910.20190920-0707
*-----------------------------------------------------------------------------
$PACKAGE ST.TemenosHackathon
*
*Implementation of ST_TemenosHackathon.tTransactionSignStoreFields
*
SUBROUTINE JWT.TRANSACTION.SIGN.STORE.FIELDS
* Developed By	:
* Program Name	: JWT.TRANSACTION.SIGN.STORE.FIELDS
* Module Name	: ST
* @package 		: ST_TemenosHackathon
*-----------------------------------------------------------------------------
* <desc>
* Program Description
* Fields Definition for JWT.TRANSACTION.SIGN.STORE
* </desc>
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------
*
*17/10/2019 -  Enhancement Temenos - Hackathon
*				Fields Definition for JWT.TRANSACTION.SIGN.STORE
*
*-----------------------------------------------------------------------------
* <region name= Inserts>
	$USING EB.Template
	$USING EB.SystemTables

* </region>
*-----------------------------------------------------------------------------
*JWT.TRANSACTION.SIGN.STORE table id
    EB.Template.TableDefineid('TRANSACTION.SIGN.STORE.ID',EB.Template.T24String)

*JWT.TOKEN
    FieldName = 'JWT.TOKEN'
    FieldLength = 1000
    FieldType = ''
    FieldType<1> = 'ANY'
    FieldType<3> = 'MANDATORY'
    Neighbour = ''
    EB.Template.TableAddfielddefinition(FieldName,FieldLength,FieldType,Neighbour)

*PUBLIC.KEY
    FieldName = 'PUBLIC.KEY'
    FieldLength = 1000
    FieldType = ''
    FieldType<1> = 'ANY'
    FieldType<3> = 'MANDATORY'
    Neighbour = ''
    EB.Template.TableAddfielddefinition(FieldName,FieldLength,FieldType,Neighbour)

*Reserved fields
    FieldName = 'RESERVED.10'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.9'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.8'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.7'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.6'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.5'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.4'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.3'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.2'
    EB.Template.TableAddreservedfield(FieldName)
    FieldName = 'RESERVED.1'
    EB.Template.TableAddreservedfield(FieldName)

* Local Ref field
    Neighbour =''
    EB.Template.TableAddlocalreferencefield(Neighbour)

* Override Field
    EB.Template.TableAddoverridefield()

*-----------------------------------------------------------------------------
    EB.Template.TableSetauditposition() ;* Audit fields
*-----------------------------------------------------------------------------

RETURN
END

